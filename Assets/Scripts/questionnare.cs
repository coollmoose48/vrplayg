﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.IO;
using UnityEngine.Networking;
using System.Net;



public class questionnare : MonoBehaviour
{
    
    public GameObject answerScreen;
    public GameObject questionText;

    string answersFromWeb, questionsFromWeb;
    public string getQuestionnare;
    public string getAnswers;

    public string[] questionList;
    public string[] answerList;

    int questionNumber;
    int readLineIndex;
    int rightAnswerCount = 0;
    //public List<bool> questionHasBeenAnswered;
    bool allQuestionsAnswered = false;
    int answerFromTrigger;

    

    

    


    // Start is called before the first frame update
    void Start()
    {
        getAnswers = "http://www.stonemoose.com/files/questionnare1.txt"; //EditorPrefs.GetString("linktoanswers");
        getQuestionnare = "http://www.stonemoose.com/files/answer1.txt"; //EditorPrefs.GetString("linktoquestionnare");

        questionText.gameObject.GetComponent<Text>().text ="";
        GetTextFromWeb();
        readLineIndex = 0;
        questionNumber = 0;
        AskQuestion(questionNumber);
        string answerText = "Question " + questionNumber + " / " + answerList.Length + "\n" + "Right answers: " + rightAnswerCount;
        answerScreen.gameObject.GetComponent<Text>().text = answerText;
        

    }
       

    void AskQuestion(int questionNumber)
    {
        if (!allQuestionsAnswered)
        {
           questionText.gameObject.GetComponent<Text>().text = questionText.gameObject.GetComponent<Text>().text + questionList[questionNumber] + "\n";
          
        }
        

    }

    public int FindAnswerFromAnswerSheet(int questionNumber)
    {
        int answer = 0;
        int.TryParse(answerList[questionNumber], out answer);
        return answer;
    }

    public void CheckAnswer(int answer)
    {
        if (!allQuestionsAnswered)
        {
            
            int rightAnswer = FindAnswerFromAnswerSheet(questionNumber);
            if (answer == rightAnswer)
            {
                rightAnswerCount++;
                questionNumber++;
                string answerText = "Question " + questionNumber + " / " + answerList.Length + "\n" + "Right answers: " + rightAnswerCount;
                answerScreen.gameObject.GetComponent<Text>().text = answerText;


            }
            else
            {
                questionNumber++;
                string answerText = "Question " + questionNumber + " / " + answerList.Length + "\n" + "Right answers: " + rightAnswerCount;
                answerScreen.gameObject.GetComponent<Text>().text = answerText;

            }
            NextQuestion();
        }

    }

    void CheckIfLastQuestion()
    {
        if(questionNumber == questionList.Length)
        {
            allQuestionsAnswered = true;
            questionText.gameObject.GetComponent<Text>().text = "All questions answered";

        }
        AskQuestion(questionNumber);
    }

    void NextQuestion()
    {
        
        if (!allQuestionsAnswered)
        {

            questionText.gameObject.GetComponent<Text>().text = "";


        }
        CheckIfLastQuestion();
    }

    private void GetTextFromWeb()
    {
        WebClient client = new WebClient();
        questionsFromWeb = client.DownloadString(getQuestionnare);
        answersFromWeb = client.DownloadString(getAnswers);
        string pattern = @"-EOQ-";
        questionList = System.Text.RegularExpressions.Regex.Split(questionsFromWeb, pattern);
        answerList = System.Text.RegularExpressions.Regex.Split(answersFromWeb, pattern);
        
    }
    
}
    






