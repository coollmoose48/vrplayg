﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public bool movement = false;
    public GameObject moveToObject;
    Vector3 moveToLocation;

    public bool interaction = false;
    public GameObject interactWithObject;

    public int triggerValueInt;

    private void Start()
    {
        if (movement && interaction)
        {
            GetComponent<Trigger>().movement = false;
            GetComponent<Trigger>().interaction = false;

        }
        if (movement)
        {
            moveToLocation = moveToObject.gameObject.transform.position;
            gameObject.tag = "trigger";
        }

        if(interaction)
        {
            gameObject.tag = "interaction";
           
        }

        

        
    }


    public void InteractWithObject(bool interacted)
    {
        if (interacted == true) 
        {
            GiveAnswer(triggerValueInt);
            Debug.Log("vastauksen arvo: " + triggerValueInt);
        }
    }

    void GiveAnswer(int answer)
    {
        interactWithObject.GetComponent<questionnare>().CheckAnswer(answer); 
    }

    public void DisableTrigger()
    {
        GetComponent<Trigger>().movement = false;
        GetComponent<Trigger>().interaction = false;
    }
}
