﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GazeControl : MonoBehaviour
{

    bool buttonPressed = false;
    public GameObject player;
    public GameObject focusPoint;
    public float focusPointSpeed = 0.1f;
    public int distanceOfRay = 50;
    private RaycastHit _hit;
    public GameObject startPoint;
    //public List<GameObject> triggers;
    Vector3 offset = new Vector3(0, 1.8f, 0);

    

    //public float focusTimerTime = 2.0f;
    private Vector3  scaleChange,positionChange;



    // Start is called before the first frame update
    void Start()
    {
        scaleChange = new Vector3(-focusPointSpeed, -focusPointSpeed, -focusPointSpeed);//Focuspoint gets smaller and when it vanishes, action is taken
        Vector3 startPosition = startPoint.transform.position;
        player.transform.position = startPosition;
         
    }
    // Update is called once per frame
    void Update()
    {

        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0.5f));
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * distanceOfRay, Color.green);
        if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
        {
            TriggerPressed();
        }

        if (Physics.Raycast(ray, out _hit, distanceOfRay)) //Sending Ray from player to check where gaze hits. If GO has tag trigger, then action.
        {
            if (_hit.transform.gameObject.tag == "trigger")
            {
                _hit.transform.gameObject.GetComponent<Renderer>().material.color = Color.green;

                focusPoint.transform.localScale += scaleChange;
                if (focusPoint.transform.localScale.x < 0.0f)
                {
                    focusPoint.transform.localScale = new Vector3(10.0f, 10.0f, 1.0f);
                    Vector3 target = _hit.transform.GetComponent<Trigger>().moveToObject.gameObject.transform.position;
                    MoveToaSpawnPoint(target);
                }
            }



            if (_hit.transform.gameObject.tag == "interaction")
            {
                
                _hit.transform.gameObject.GetComponent<Renderer>().material.color = Color.blue;
                focusPoint.transform.localScale += scaleChange;
                if (focusPoint.transform.localScale.x < 0.0f)
                {
                    focusPoint.transform.localScale = new Vector3(10.0f, 10.0f, 1.0f);
                    Interaction();
                }

            }
        }
        //else focusPoint.transform.localScale = new Vector3(10.0f, 10.0f, 1.0f); //if no triggers available, keep focuspoint bigger
    }    

        private void MoveToaSpawnPoint(Vector3 spawnPoint)
    {
        player.transform.position = spawnPoint;


    }
    
   private void TriggerPressed()
    {
        
        player.transform.position = startPoint.gameObject.transform.position;
    }

    private void Interaction()
    {
        GameObject interactWithTrigger = _hit.transform.GetComponent<Trigger>().gameObject;

        if (_hit.transform.gameObject.tag == "interaction")
        {
            Debug.Log("button pressed " + _hit.transform.gameObject.GetComponent<Trigger>().triggerValueInt);
            buttonPressed = true;
            interactWithTrigger.GetComponent<Trigger>().InteractWithObject(buttonPressed);
            buttonPressed = false;

        }
        
        
    }
}
