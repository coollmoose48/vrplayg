Concept
a hospital and/or school environment for students to learn different tasks for nursing school students. 

Initial Design
Student starts from Orientation and gets instructions on how to operate with VR gear and what are the contents 
of the lessons in each room. After Orientation student moves to the hallway and chooses a room or when one room
 is completed, student moves to next.  Every user should have account based information, so that every time 
when user logs in and has maybe completed a room, user should see what rooms user has completed. they will 
have some sort of evaluation system when completing rooms. If there is enough time, tools for adding content
 for master users should be implemented.

Goal for this is to create an environment where adding content should be easy enough to create new content 
and different kind of scenarios for healtcare students. When creating content, special attention is made to 
360 still images and videos for easier content creation.