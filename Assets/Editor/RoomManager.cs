﻿using UnityEditor;
using UnityEngine;



public class MyRoomManager : EditorWindow
{
    string linkToRoom1,linkToRoom2;
    string linkToQuestionnare, linkToAnswers;
    GameObject room1, room2;
    int amountOfMedia = 1;

    private void Awake()
    {
        GetSavedData();
    }


    // Add menu item
    [MenuItem("Room Manager Tools/Room Manager")]
    public static void ShowWindow()
    {
        
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(MyRoomManager));
    }

    void OnGUI()
    {
        GUILayout.Label("Room Manager", EditorStyles.boldLabel);
        amountOfMedia = EditorGUILayout.IntField("amount of media files", amountOfMedia);
        room1 = EditorGUILayout.ObjectField(room1, typeof(GameObject), true) as GameObject;
        for(int a = 0; a < amountOfMedia; a++)
        {
            linkToRoom1 = EditorGUILayout.TextField("room media here(link)", linkToRoom1);
        }
        

        EditorGUILayout.Space();
        room2 = EditorGUILayout.ObjectField(room2, typeof(GameObject), true) as GameObject;
        for (int a = 0; a < amountOfMedia; a++)
        {
            linkToRoom2 = EditorGUILayout.TextField("room media here(link)", linkToRoom2);
        }
        
        EditorGUILayout.Space();
        linkToQuestionnare = EditorGUILayout.TextField("Http link to questions here", linkToQuestionnare);
        linkToAnswers = EditorGUILayout.TextField("Http link to answers here", linkToAnswers);
        EditorGUILayout.Space();
        
        

        if (GUILayout.Button("Save settings", EditorStyles.miniButton))
        {
            SaveSettings();
            
        }

    }

    

    public void SaveSettings()
    {
        //EditorPrefs.SetString("linktoquestionnare", linkToQuestionnare);
        //EditorPrefs.SetString("linktoanswers", linkToAnswers);
        //Debug.Log("pressed update");
        //Debug.Log(room1.name);
        //room1.gameObject.GetComponent<RoomController>().room = room1;
        //room2.gameObject.GetComponent<RoomController>().room = room2;

    }

    public void GetSavedData()
    {
        linkToQuestionnare = EditorPrefs.GetString("linktoquestionnare");
        linkToAnswers = EditorPrefs.GetString("linktoanswers");
        

    }

    
}