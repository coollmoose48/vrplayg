﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class RoomController : MonoBehaviour
{
    public GameObject canvasForMaterial;
    public Material roomMaterial;
    public VideoClip videoClip;
    
    public bool Video = false;
    public bool Image = false;
    public Material defaultMaterial;

    
    
   
    // Start is called before the first frame update
    void Start()
    {
        
        if(Video && Image)
        {
            canvasForMaterial.GetComponent<VideoPlayer>().enabled = false;
            canvasForMaterial.GetComponent<MeshRenderer>().enabled = false;
        }
        if (Image)
        {
            canvasForMaterial.GetComponent<MeshRenderer>().material = roomMaterial;
        }
        
        if (Video)
        {
            canvasForMaterial.GetComponent<VideoPlayer>().clip = videoClip;

        }
        else canvasForMaterial.GetComponent<VideoPlayer>().enabled = false;
        

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
